#!/bin/bash

# -r - delete working directory after build
# -v - verbose output
# -w - work directory
# -o - output directory
sudo ./$(dirname $0)/mkarchiso -r -v -w /tmp/archiso_build -o deploy $(dirname $0)
